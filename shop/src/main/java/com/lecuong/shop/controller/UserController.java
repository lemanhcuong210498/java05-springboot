package com.lecuong.shop.controller;

import com.lecuong.shop.model.request.user.UserFilterRequest;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import com.lecuong.shop.model.response.ListResponse;
import com.lecuong.shop.model.response.ObjectResponse;
import com.lecuong.shop.model.response.user.UserDetailResponse;
import com.lecuong.shop.service.UserService;
import com.lecuong.shop.validate.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ObjectResponse saveUser(@RequestBody UserSaveRequest userSaveRequest) {
        userSaveRequest = UserValidator.validateUserSave(userSaveRequest);
        userService.save(userSaveRequest);
        return ok();
    }

    @GetMapping("/{id}")
    public ObjectResponse getUserById(@PathVariable Long id){
        UserDetailResponse detailResponse = userService.findById(id);
        return ok(detailResponse);
    }

    @GetMapping
    public ListResponse getAll(@ModelAttribute UserFilterRequest userFilterRequest) {
        ListResponse listResponse = userService.findAll(userFilterRequest);
        return ok(listResponse);
    }
}

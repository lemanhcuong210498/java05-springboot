package com.lecuong.shop.repository;

import com.lecuong.shop.entity.User;
import com.lecuong.shop.model.response.UserAddressResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {

    User findByUserNameAndPassword(String userName, String  password);

    List<User> findByUserNameContaining(String userName);

    List<User> findAllByAddress(String address);

    long countAllByAddress(String address);

    @Query(value = "SELECT new com.lecuong.shop.model.response.UserAddressResponse(u.address, count(u.id)) FROM User u GROUP BY u.address")
    List<UserAddressResponse> findAllByAddress();

}

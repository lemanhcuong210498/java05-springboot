package com.lecuong.shop.model.response.user;

import com.lecuong.shop.model.response.role.RoleResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    private Long id;
    private String userName;
    private String address;
    private String email;
    private String phone;
    private String name;
}

package com.lecuong.shop.service;

import com.lecuong.shop.entity.User;
import com.lecuong.shop.model.request.user.UserFilterRequest;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import com.lecuong.shop.model.response.ListResponse;
import com.lecuong.shop.model.response.UserAddressResponse;
import com.lecuong.shop.model.response.user.UserDetailResponse;
import com.lecuong.shop.model.response.user.UserResponse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    List<UserAddressResponse> getAll();

//    Page<User> getAll(UserFilterRequest userFilterRequest);

    void save(UserSaveRequest userSaveRequest);

    UserDetailResponse findById(long id);

    ListResponse<UserResponse> findAll(UserFilterRequest userFilterRequest);
}

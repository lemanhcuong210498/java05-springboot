package com.lecuong.shop.service.impl;

import com.lecuong.shop.entity.User;
import com.lecuong.shop.exception.ObjectNotFoundException;
import com.lecuong.shop.mapper.UserMapper;
import com.lecuong.shop.model.request.user.UserFilterRequest;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import com.lecuong.shop.model.response.ListResponse;
import com.lecuong.shop.model.response.UserAddressResponse;
import com.lecuong.shop.model.response.user.UserDetailResponse;
import com.lecuong.shop.model.response.user.UserResponse;
import com.lecuong.shop.repository.UserRepository;
import com.lecuong.shop.repository.specification.UserSpecification;
import com.lecuong.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserAddressResponse> getAll() {
        return userRepository.findAllByAddress();
    }

    @Override
    public void save(UserSaveRequest userSaveRequest) {
        User user = userMapper.mapToEntity(userSaveRequest);
        userRepository.save(user);
    }

    @Override
    public UserDetailResponse findById(long id) {
        Optional<User> user = userRepository.findById(id);
        user.orElseThrow(() -> new ObjectNotFoundException(MessageFormat.format("User not found with Id: {0}", id)));
        return userMapper.mapToDetailResponse(user.get());
    }

    @Override
    public ListResponse<UserResponse> findAll(UserFilterRequest userFilterRequest) {

        int pageSize = userFilterRequest.getPageSize();
        int pageIndex = userFilterRequest.getPageIndex();

        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        Page<User> users = userRepository.findAll(UserSpecification.filter(userFilterRequest), pageRequest.previousOrFirst());

        List<UserResponse> userResponses = users.stream().map(userMapper::mapToResponse).collect(Collectors.toList());

        return ListResponse.of(users.getTotalElements(),userResponses);
    }

    //    @Override
//    public Page<User> getAll(UserFilterRequest userFilterRequest){
//        PageRequest pageRequest = PageRequest.of(userFilterRequest.getPageIndex(), userFilterRequest.getPageSize());
//        return userRepository.findAll(UserSpecification.filter(userFilterRequest), pageRequest.previousOrFirst());
//    }
}

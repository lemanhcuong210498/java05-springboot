package com.lecuong.shop.model.response.role;

import lombok.Data;

@Data
public class RoleResponse {

    private Long id;
    private String description;
    private String name;
}

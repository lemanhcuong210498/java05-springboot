package com.lecuong.shop.model.response.user;

import com.lecuong.shop.model.response.role.RoleResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailResponse {

    private Long id;
    private String userName;
    private String address;
    private String email;
    private String phone;
    private String name;
    private List<RoleResponse> roles;
}

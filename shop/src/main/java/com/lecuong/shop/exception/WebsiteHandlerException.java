package com.lecuong.shop.exception;

import com.lecuong.shop.model.response.ObjectResponse;
import com.lecuong.shop.model.response.StatusResponse;
import com.lecuong.shop.validate.ValidateException;
import org.hibernate.HibernateException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;

@RestControllerAdvice
public class WebsiteHandlerException extends ResponseEntityExceptionHandler {

    @ExceptionHandler(HibernateException.class)
    public ObjectResponse handlerHibernateException(HibernateException e) {
        return error("HIBERNATE", e.getMessage(), 400);
    }

    @ExceptionHandler(ValidateException.class)
    public ObjectResponse handlerValidator(ValidateException val) {
        return error(Status.VALIDATE, val.getMessage(), 400);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ObjectResponse handlerAccessDeniedException(AccessDeniedException accessDeniedException) {
        return error(Status.ACCESS_DENIED, accessDeniedException.getMessage(), 403);
    }

    @ExceptionHandler(InternalException.class)
    public ObjectResponse handlerAccessDeniedException(InternalException internalException) {
        return error(Status.INTERNAL, internalException.getMessage(), 403);
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ObjectResponse handlerObjectNotFoundException(ObjectNotFoundException val) {
        return error(Status.OBJECT_NOT_FOUND, val.getMessage(), 404);
    }

    public ObjectResponse error(String code, String message, int httpStatus) {
        ObjectResponse objectResponse = setStatus(null, code, message, httpStatus);
        return objectResponse;
    }

    public ObjectResponse setStatus(Object data, String code, String message, int httpStatus) {
        StatusResponse status = new StatusResponse();
        status.setCode(code);
        status.setMessage(message);
        status.setStatus(httpStatus);

        return new ObjectResponse(status, data);
    }
}

package com.lecuong.shop.model.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatusResponse {

    private String code;
    private String message;
    private int status;
}

package com.lecuong.shop.validate;

import com.lecuong.shop.model.request.user.UserSaveRequest;
import org.apache.commons.lang3.StringUtils;

public class UserValidator {

    public static UserSaveRequest validateUserSave(UserSaveRequest userSaveRequest) {
        return Validator.of(userSaveRequest)
                .validate(UserSaveRequest::getUserName, UserValidator::invalidUserName, () -> new ValidateException("UserName can't empty"))
                .validate(UserSaveRequest::getPassword, UserValidator::invalidPassword, () -> new ValidateException("Password can't empty"))
                .validate(UserSaveRequest::getEmail, UserValidator::invalidEmail, () -> new ValidateException("Email can't empty"))
                .validate(UserSaveRequest::getPhone, UserValidator::invalidPhone, () -> new ValidateException("Phone can't empty"))
                .get();
    }

    private static boolean invalidUserName(String userName) {
        return StringUtils.isEmpty(userName);
    }

    private static boolean invalidPassword(String password) {
        return StringUtils.isEmpty(password);
    }

    private static boolean invalidEmail(String email) {
        return StringUtils.isEmpty(email);
    }

    private static boolean invalidPhone(String phone) {
        return StringUtils.isEmpty(phone);
    }
}

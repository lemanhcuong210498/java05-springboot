package com.lecuong.shop.mapper;

import com.lecuong.shop.entity.Role;
import com.lecuong.shop.model.response.role.RoleResponse;
import com.lecuong.shop.utils.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {

    public RoleResponse mapToResponse(Role role){
        RoleResponse roleResponse = new RoleResponse();
        BeanUtils.refine(role, roleResponse, BeanUtils::copyNonNull);
        return roleResponse;
    }

}
